---
title: "Lab 4.5: Setup the GitOps CD Pull Agent"
weight: 45
chapter: true
draft: false
description: "See GitLab GitOps CD pull deployment and configuration management in action."
#pre: '<i class="fa fa-film" aria-hidden="true"></i> '
---

# Lab 4.5: Setup the GitOps CD Pull Agent

> **Keyboard Time**: 10 mins, **Automation Wait Time**: 5 mins
>
> **Scenarios:** Instructor-Led, Self-Paced

{{< admonition type=gitops title="GitOps Conventions" open=true >}}

1. Monitoring manifests by an agent running in a Kubernetes cluster. This lab configures the GitLab Agent to monitor the manifests in this repository.

{{< /admonition >}}

{{< admonition type=abstract title="Target Outcomes" open=true >}}

1. Configure the Kubernetes Agent to monitor the CI constructed kubernetes manifests.

2. Observe the initial deployment of staging and production via the appearance of the target environments.

   {{< /admonition >}}

{{< admonition type=warning title="Warning" open=true >}}

**For instructor-led classes, this portion will be done by the instructor.**

**If you are not in an instructor-led course, perform the lab as described.**

{{< /admonition >}}

{{< admonition type=quote title="Done By Instructor for Instructor-Led Courses" open=true >}}

1. Logon the cluster administration machine => [Instructions for SSM Session Manager for EKS]({{< relref "../090_appendices/tuning_and_troubleshooting.md#using-the-eks-bastion-for-cluster-administration-with-kubectl-and-helm" >}})

2. Run the following command to install flux in the cluster:
   
   ```bash
   flux install

   flux check
   ```

3. Create a GitRepository object and apply it to the cluster replacing _classgroup_ and _yourpersonalgroup_ with the actual names for your project.

   ```
   kubectl apply -f - <<EOF
     apiVersion: source.toolkit.fluxcd.io/v1
     kind: GitRepository
     metadata:
      name: world-greetings-env-1
      namespace: flux-system
     spec:
       interval: 1m0s
       url: https://gitlab.com/_classgroup_/_yourpersonalgroup_/world-greetings-env-1
       ref:
         branch: main
   EOF

   ```

4. Ensure the repo has successfully synced in the cluster. You can do that by looking at the GitRepository object in the flux-system namespace.

   ```
   kubectl get GitRepository -n flux-system
   ```

5. Create a kustomization object to track and apply the manifests in your world-greetings-env-1 repository.

   ```
   kubectl apply -f - <<EOF
     apiVersion: kustomize.toolkit.fluxcd.io/v1
     kind: Kustomization
     metadata:
       name: world-greetings
       namespace: flux-system
     spec:
       interval: 1m0s
       path: ./manifests/
       prune: true
       sourceRef:
         kind: GitRepository
         name: world-greetings-env-1
   EOF
   ```

6. Watch the state of the world-greetings kustomization object in the cluster.

   `watch kubectl get kustomization -n flux-system`
    
    <mark class="hlgreen">Leave this view open as you will be instructed to consult it to see the state of the kustomization object when the GitLab Agent pulls and processes the kubernetes manifests.</mark>

{{< /admonition >}}

11. Watch the flux kustomization objects in the cluster to validate the state of your deployed applications.

   `watch kubectl get kustomization -n flux-system`
    
    <mark class="hlgreen">**For Instructor-Led**: the instructor may have this view displayed for everyone</mark>
    
12. To watch the progress, navigate to ***classgroup/yourpersonalgroup*/world-greetings-env-1**

13. *Click* **Deployments => Environments**

14. **[Automation wait: ~3 min]** Keep refreshing until staging deployment activities complete.

   {{< admonition type=warning title="Warning" open=true >}}
   For all GitOps mode projects, when the deployment shows complete in the Environments page, it only means the manifests are completely setup, the Gitlab Agent for Kubernetes still has to find and deploy the changed manifests
   {{< /admonition >}}

15. **[Automation wait: ~3 min]** Wait after the status shows complete…

16. On the ‘staging’ line, to the right, *Click* **Open**

   > You can see the staging deployed application.

17. In the browser tabs, *Click* **[the tab with the Environments page]**

18. On the ‘production’ line, to the right, *Click* **Open**

   > You can see the production deployed application

19. If there an error indicating there is no site yet, keep refreshing the browser window until the site displays.

{{< admonition type=warning title="Warning" open=true >}}

For all GitOps mode projects, when the deployment shows complete in the Environments page, it only means the manifests are completely setup, the Gitlab Agent for Kubernetes still has to find and deploy the changed manifests. Also note that on the very first time the agent is configured to monitor your manifests - all environments are deployed. From this point forward the manifests will be updated sequentially and will require approval for production.

{{< /admonition >}}

{{< admonition type=danger title="Critical Mindfulness: Only Pull Deployment In Environment Deployment Project" open=true >}}

Subsequent labs will be adding many Runner Based jobs to enable security scanning and dynamic environments. However, the deployment of this application will always be accomplished by a Pull Deployment through the GitLab Agent as you have seen in this lab. You may consult the job log (to see a manifest commit only) and/or the Kubernetes Agent log to verify this.

{{< /admonition >}}

{{< admonition type=success title="Accomplished Outcomes" open=true >}}

1. Configure the Kubernetes Agent to monitor the CI constructed kubernetes manifests.

2. Observe the initial deployment of staging and production via tailing the Kubernetes Agent log and the appearance of the target environments.

   {{< /admonition >}}
